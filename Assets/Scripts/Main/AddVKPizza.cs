﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddVKPizza : MonoBehaviour
{
    string id;

    public string Id { get => id; set => id = value; }

    public void AddCorzTovar()
    {
        var mc = GameObject.Find("MainController").GetComponent<MainCorzina>();
        mc.AddValue(Id);
    }
}
