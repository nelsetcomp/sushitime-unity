﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoOform : MonoBehaviour
{
    public GameObject c1;
    public GameObject c2;


    public void Go()
    {
        var m = GameObject.Find("MainController").GetComponent<MainCorzina>().CenaPr;

        if(m >= int.Parse(GameObject.Find("Hud").GetComponent<Hud>()._Minorder))
        {
            c1.SetActive(false);
            c2.SetActive(true);
        }
    }
    public void Back()
    {
        c1.SetActive(true);
        c2.SetActive(false);
    }
}
