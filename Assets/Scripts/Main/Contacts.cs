﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Contacts : MonoBehaviour
{
   
    void Start()
    {
        string t = "";
        switch (GameObject.Find("Hud").GetComponent<Hud>()._City)
        {
            case "Михайловск":
                t = "Адрес - ул. Ишкова 149" +
                    "\nТелефон - +7 (8652) 21-17-31";
                break;
            default:
                t = "Адрес - ул. Куйбышева 48б" +
                    "\nТелефон - +7 (8652) 62-30-50";
                break;
        }
        GetComponent<Text>().text = t;
    }

}
