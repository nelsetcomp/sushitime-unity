﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AddCityDrop : MonoBehaviour
{
    void Start()
    {
        List<string> lis = new List<string>();


        foreach (var r in GameObject.Find("Hud").GetComponent<Hud>().CityList)
        {
            lis.Add(r);
        }
        var d = GetComponent<Dropdown>();
        d.options.Clear();
        d.AddOptions(lis);
        for (int x = 0; x < lis.Count; x++)
        {
            if(lis[x] == PlayerPrefs.GetString("city"))
            {
                d.value = x;
            }
        }
    }
    public void CityChange()
    {
        var _d = GetComponent<Dropdown>();
        if (_d.options[_d.value].text != PlayerPrefs.GetString("city")) {
            PlayerPrefs.SetString("city", _d.options[_d.value].text);

            var hud = GameObject.Find("Hud");
            Destroy(hud);
            SceneManager.LoadScene("Start");
        }
    } 
}
