﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCorzina : MonoBehaviour
{
    List<string> mainTovar = new List<string>();
    Dictionary<string, MyK> dicTovar = new Dictionary<string, MyK>();
    public List<string> MainTovar { get => mainTovar; set => mainTovar = value; }
    public Dictionary<string, MyK> DicTovar { get => dicTovar; set => dicTovar = value; }
    public int CenaPr { get => cenaPr; set => cenaPr = value; }

    public GameObject content;
    public GameObject RollPrefab;
    public GameObject SetsPrefab;
    public Text cena;
    public GameObject corzImage;
    private int cenaPr;
    public class MyK{
        int count;
        string name;
        int price;
        string composition;
        string menu;

        public int Count { get => count; set => count = value; }
        public string Name { get => name; set => name = value; }
        public int Price { get => price; set => price = value; }
        public string Composition { get => composition; set => composition = value; }
        public string Menu { get => menu; set => menu = value; }
    }
    private void Start()
    {
        SummPrice();
    }
    public void SummPrice()
    {
        int c = 0;
       
        foreach (var t in MainTovar)
        {
            foreach (var h in GameObject.Find("Hud").GetComponent<Hud>()._Alltovar)
            {
                
                if (h.Id == t)
                {
                    c += int.Parse(h.Price); 
                }
                if (t == h.Id+":")
                {
                    c += int.Parse(h.PriceA);
                }
                if (t == h.Id + "::")
                {
                    c += int.Parse(h.PriceB);
                }
            }
        }
        if(c >= int.Parse(GameObject.Find("Hud").GetComponent<Hud>()._Minorder))
        {
            cena.text = "Заказать " + c + " ₽";
        }
        else
        {
            cena.text = "Минимальный заказ " + GameObject.Find("Hud").GetComponent<Hud>()._Minorder + " ₽";
        }
        if(MainTovar.Count == 0)
        {
            corzImage.SetActive(true);
        }
        if (MainTovar.Count > 0)
        {
            corzImage.SetActive(false);
        }
        var topCount = GameObject.Find("topCount").GetComponent<Text>();
        topCount.text = c + " ₽ | " + MainTovar.Count;
        CenaPr = c;
    }
    public void AddValue(string id)
    {
        MainTovar.Add(id);
        SummPrice();
        SumCorz();
    }
    public void ClearCorz()
    {
        MainTovar.Clear();
        DicTovar.Clear();
        foreach (Transform child in content.transform)
        {
            Destroy(child.gameObject);
        }
        SummPrice();
    }
    void SumCorz()
    {
        DicTovar.Clear();
        foreach (Transform child in content.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (var t in MainTovar)
        {
            foreach (var h in GameObject.Find("Hud").GetComponent<Hud>()._Alltovar)
            {
                var r = t.Replace(":", "");
                if (h.Id == r)
                {
                    if (DicTovar.ContainsKey(t))
                    {
                        DicTovar[t].Count += 1;
                    }
                    else
                    {
                        if (h.Composition != "0")
                        {
                            if (t == h.Id + ":")
                            {
                                DicTovar.Add(t, new MyK { Count = 1, Name = h.Name + " 30 см.", Composition = h.Composition, Price = int.Parse(h.PriceA), Menu = h.Menu });
                            }
                            if (t == h.Id + "::")
                            {
                                DicTovar.Add(t, new MyK { Count = 1, Name = h.Name + " 40 см.", Composition = h.Composition, Price = int.Parse(h.PriceB), Menu = h.Menu });
                            }
                            if (t == h.Id)
                            {
                                DicTovar.Add(t, new MyK { Count = 1, Name = h.Name, Composition = h.Composition, Price = int.Parse(h.Price), Menu = h.Menu });
                            }
                        }
                        else
                        {
                            DicTovar.Add(t, new MyK { Count = 1, Name = h.Name, Composition = "1 литр", Price = int.Parse(h.Price), Menu = h.Menu });
                        }
                    }
                }
            }
        }

        foreach (var d in DicTovar)
        {
            GameObject op;
            switch (d.Value.Menu)
            {
                case "sets":
                    op = SetsPrefab;
                    break;
                case "wok":
                    op = SetsPrefab;
                    break;
                default:
                    op = RollPrefab;
                    break;
            }
            var scrollItem = Instantiate(op);
            scrollItem.transform.SetParent(content.transform, false);
            scrollItem.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Text>().text = d.Value.Name;
            scrollItem.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Text>().text = d.Value.Price * d.Value.Count + " ₽";
            scrollItem.transform.GetChild(0).transform.GetChild(2).transform.GetComponent<Text>().text = d.Value.Composition;
            scrollItem.transform.GetChild(0).transform.GetChild(5).transform.GetComponent<Text>().text = d.Value.Count+"";
            scrollItem.transform.GetChild(0).transform.GetChild(3).transform.GetComponent<AdBScr>().Id = d.Key;
            scrollItem.transform.GetChild(0).transform.GetChild(4).transform.GetComponent<DelBScr>().Id = d.Key;
            if (GameObject.Find("MainController").GetComponent<MainController>().menuAtlas.GetSprite(d.Key.Replace(":", "")) != null)
            {
                scrollItem.transform.GetChild(0).transform.GetChild(6).transform.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().menuAtlas.GetSprite(d.Key.Replace(":", ""));
            }
        }
    }
}
