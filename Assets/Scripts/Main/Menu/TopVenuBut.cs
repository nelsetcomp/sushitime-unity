﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using UnityEngine.UI;

public class TopVenuBut : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public GameObject b1;
    public GameObject b2;
    public GameObject b3;
    public GameObject b4;
    public GameObject b5;
    public GameObject b6;
    public GameObject b7;
    public GameObject b8;
    public GameObject b9;

    public GameObject autoScroll;
    public GameObject m;
    public GameObject mEnuSwipe;

    public float value;
    public float Mvalue;
    public int state;

    public void OnPointerClick(PointerEventData eventData)
    {
        Clic();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
    
    public void Clic()
    {
        mEnuSwipe.GetComponent<SwipeDetector>().state = state;
        autoScroll.GetComponent<AutoScroll>().Go(value);
        m.GetComponent<MoveM>().Movem(Mvalue);

        transform.GetChild(0).transform.GetComponent<Text>().color = Color.black;
        transform.GetChild(1).transform.GetComponent<Image>().enabled = true;

        
        UnClic(b1);
        UnClic(b2);
        UnClic(b3);
        UnClic(b4);
        UnClic(b5);
        UnClic(b6);
        UnClic(b7);
        UnClic(b8);
        UnClic(b9);
    }
   
    void UnClic(GameObject go)
    {
        go.transform.GetChild(0).transform.GetComponent<Text>().color = new Color(0.509804f, 0.5058824f, 0.509804f, 1);
        go.transform.GetChild(1).transform.GetComponent<Image>().enabled = false;
    }
}
