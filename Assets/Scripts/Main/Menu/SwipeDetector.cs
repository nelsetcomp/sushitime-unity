﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;

    public float SWIPE_THRESHOLD = 20f;
    public int state = 0;
    public GameObject m0;
    public GameObject m1;
    public GameObject m2;
    public GameObject m3;
    public GameObject m4;
    public GameObject m5;
    public GameObject m6;
    public GameObject m7;
    public GameObject m8;
    public GameObject m9;

    public RectTransform touchArea;
    private RaycastHit hit;

    //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
    void OnSwipeUp()
    {
        Debug.Log("Swipe UP");
        
    }

    void OnSwipeDown()
    {
        Debug.Log("Swipe Down");
    }

    void OnSwipeLeft()
    {
        Debug.Log("Swipe Left");
        state++;
        if (state > 9) state = 9;
        switch (state)
        {
            case 0:
                m0.GetComponent<TopVenuBut>().Clic();
                break;
            case 1:
                m1.GetComponent<TopVenuBut>().Clic();
                break;
            case 2:
                m2.GetComponent<TopVenuBut>().Clic();
                break;
            case 3:
                m3.GetComponent<TopVenuBut>().Clic();
                break;
            case 4:
                m4.GetComponent<TopVenuBut>().Clic();
                break;
            case 5:
                m5.GetComponent<TopVenuBut>().Clic();
                break;
            case 6:
                m6.GetComponent<TopVenuBut>().Clic();
                break;
            case 7:
                m7.GetComponent<TopVenuBut>().Clic();
                break;
            case 8:
                m8.GetComponent<TopVenuBut>().Clic();
                break;
            case 9:
                m9.GetComponent<TopVenuBut>().Clic();
                break;
        }
    }

    void OnSwipeRight()
    {
        Debug.Log("Swipe Right");
        state--;
        if (state < 0) state = 0;
        switch (state)
        {
            case 0:
                m0.GetComponent<TopVenuBut>().Clic();
                break;
            case 1:
                m1.GetComponent<TopVenuBut>().Clic();
                break;
            case 2:
                m2.GetComponent<TopVenuBut>().Clic();
                break;
            case 3:
                m3.GetComponent<TopVenuBut>().Clic();
                break;
            case 4:
                m4.GetComponent<TopVenuBut>().Clic();
                break;
            case 5:
                m5.GetComponent<TopVenuBut>().Clic();
                break;
            case 6:
                m6.GetComponent<TopVenuBut>().Clic();
                break;
            case 7:
                m7.GetComponent<TopVenuBut>().Clic();
                break;
            case 8:
                m8.GetComponent<TopVenuBut>().Clic();
                break;
            case 9:
                m9.GetComponent<TopVenuBut>().Clic();
                break;
        }
       
    }
    // Update is called once per frame
    bool IsPointInRT(Vector2 point, RectTransform rt)
    {
        // Get the rectangular bounding box of your UI element
        Rect rect = rt.rect;
        print(rt.anchoredPosition.x);
        // Get the left, right, top, and bottom boundaries of the rect
        float leftSide = rt.anchoredPosition.x - rect.width/2;
        float rightSide = rt.anchoredPosition.x + rect.width/2;
        float topSide = rt.anchoredPosition.y + rect.height/2;
        float bottomSide = rt.anchoredPosition.y - rect.height/2;

        //Debug.Log(leftSide + ", " + rightSide + ", " + topSide + ", " + bottomSide);

        // Check to see if the point is in the calculated bounds
        if (point.x >= leftSide &&
            point.x <= rightSide &&
            point.y >= bottomSide &&
            point.y <= topSide)
        {
            return true;
        }
        return false;
    }
    void Update()
    {

        foreach (Touch touch in Input.touches)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            Vector2 touchPos = new Vector2(wp.x, wp.y);
            if (touchArea.GetComponent<BoxCollider2D>() == Physics2D.OverlapPoint(touchPos))
            {
             if (touch.phase == TouchPhase.Began)
                {
                    fingerUp = touch.position;
                    fingerDown = touch.position;
                }

                //Detects Swipe while finger is still moving
                if (touch.phase == TouchPhase.Moved)
                {
                    if (!detectSwipeOnlyAfterRelease)
                    {
                        fingerDown = touch.position;
                        checkSwipe();
                    }
                }

                //Detects swipe after finger is released
                if (touch.phase == TouchPhase.Ended)
                {
                    fingerDown = touch.position;
                    checkSwipe();
                }
            }
        }
    }

    void checkSwipe()
    {
        SWIPE_THRESHOLD = Screen.width*0.2f;
        //Check if Vertical swipe
        if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
        {
            //Debug.Log("Vertical");
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                OnSwipeUp();
            }
            else if (fingerDown.y - fingerUp.y < 0)//Down swipe
            {
                OnSwipeDown();
            }
            fingerUp = fingerDown;
        }

        //Check if Horizontal swipe
        else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
        {
            //Debug.Log("Horizontal");
            if (fingerDown.x - fingerUp.x > 0)//Right swipe
            {
                OnSwipeRight();
            }
            else if (fingerDown.x - fingerUp.x < 0)//Left swipe
            {
                OnSwipeLeft();
            }
            fingerUp = fingerDown;
        }

        //No Movement at-all
        else
        {
            //Debug.Log("No Swipe!");
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    
}
