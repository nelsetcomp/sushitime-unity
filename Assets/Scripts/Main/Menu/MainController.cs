﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class MainController : MonoBehaviour
{
    public SpriteAtlas menuAtlas;
    public Sprite hot;
    public Sprite spicy;
    public Sprite vegan;
    public Sprite classic;
}
