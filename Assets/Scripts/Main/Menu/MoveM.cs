﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveM : MonoBehaviour
{
    float tf = 0;
    bool bl = false;
    int napr = 0;
   public void Movem(float pos)
   {
        tf = pos;
        if (tf < GetComponent<RectTransform>().anchoredPosition.x)
        {
            napr = 1;
            bl = true;
        }
        if (tf > GetComponent<RectTransform>().anchoredPosition.x)
        {
            napr = 0;
            bl = true;
        }
        if (tf == GetComponent<RectTransform>().anchoredPosition.x)
        {
            bl = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (bl == true)
        {
            switch (napr)
            {
                case 0:
                    GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x + 60f, GetComponent<RectTransform>().anchoredPosition.y);
                    break;
                case 1:
                    GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x - 60f, GetComponent<RectTransform>().anchoredPosition.y);
                    break;
            }
            if (GetComponent<RectTransform>().anchoredPosition.x == tf)
            {
                bl = false;
            }
        }
    }
}
