﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClicM1 : MonoBehaviour, IPointerClickHandler
{
    public int id;
    public bool state;
    public GameObject a1;
    public GameObject a2;
    public GameObject a3;
    public GameObject a4;
    public GameObject a5;

    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    public void clic()
    {
        if(state == false)
        {
            state = true;
            transform.GetChild(0).gameObject.SetActive(true);
            if(a1 != null)
            {
                a1.transform.GetChild(0).gameObject.SetActive(false);
                a1.GetComponent<ClicM1>().state = false;
            }
            if (a2 != null)
            {
                a2.transform.GetChild(0).gameObject.SetActive(false);
                a2.GetComponent<ClicM1>().state = false;
            }
            if (a3 != null)
            {
                a3.transform.GetChild(0).gameObject.SetActive(false);
                a3.GetComponent<ClicM1>().state = false;
            }
            if (a4 != null)
            {
                a4.transform.GetChild(0).gameObject.SetActive(false);
                a4.GetComponent<ClicM1>().state = false;
            }
            if (a5 != null)
            {
                a5.transform.GetChild(0).gameObject.SetActive(false);
                a5.GetComponent<ClicM1>().state = false;
            }
            var pw = GameObject.Find("PriceWok").GetComponent<WOKprice>();
            pw.SumPrWok();
        }
    }
}
