﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZacazWok : MonoBehaviour
{
    string name;
    string idWok;
    int price;
    string composition;

    public string IdWok { get => idWok; set => idWok = value; }
    public string Name { get => name; set => name = value; }
    public int Price { get => price; set => price = value; }
    public string Composition { get => composition; set => composition = value; }

    public void AddCorzTovar()
    {
        print("ok");
        
        var allT = GameObject.Find("Hud").GetComponent<Hud>()._Alltovar;
        bool n = true;
        foreach(var al in allT)
        {
            if(al.Id == IdWok)
            {
                n = false;
                break;
            }
        }
        if (n == true) {
            allT.Add(new Hud.AllT { Name = Name, Id = IdWok, Price = Price + "", Composition = Composition, Menu = "wok" });
        }

        var mc = GameObject.Find("MainController").GetComponent<MainCorzina>();
        mc.AddValue(IdWok);
    }
}
