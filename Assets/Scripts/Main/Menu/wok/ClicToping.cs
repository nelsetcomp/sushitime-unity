﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClicToping : MonoBehaviour, IPointerClickHandler
{
    public int id;
    public bool state;

    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    public void clic()
    {
        if (state == false)
        {
            state = true;
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            state = false;
            transform.GetChild(0).gameObject.SetActive(false);
        }
        var pw = GameObject.Find("PriceWok").GetComponent<WOKprice>();
        pw.SumPrWok();
    }
}
