﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WOKprice : MonoBehaviour
{
    [Header("M1")]
    public ClicM1 udon;
    public ClicM1 soba;
    public ClicM1 funch;
    public ClicM1 ramen;

    [Header("M2")]
    public ClicM1 netO;
    public ClicM1 daO;

    [Header("M3")]
    public ClicM1 netS;
    public ClicM1 ostr;
    public ClicM1 slch;
    public ClicM1 tai;
    public ClicM1 ter;
    public ClicM1 firm;

    [Header("M4")]
    public ClicM4 gov;
    public ClicM4 kur;
    public ClicM4 los;
    public ClicM4 mk;
    public ClicM4 svin;

    [Header("M5")]
    public ClicToping kunsh;
    public ClicToping moza;
    public ClicToping asort;
    public ClicToping ogur;
    public ClicToping pamez;
    public ClicToping fasol;
    public ClicToping suc;
    public ClicToping sher;
    public ClicToping shamp;
    public ClicToping egg;

    [Header("M6")]
    public ClicM1 netS1;
    public ClicM1 ostr1;
    public ClicM1 slch1;
    public ClicM1 tai1;
    public ClicM1 ter1;
    public ClicM1 firm1;

    public ZacazWok zw;


    void Start()
    {
        SumPrWok();
    }

    public void SumPrWok()
    {
        var wok = GameObject.Find("Hud").GetComponent<Hud>()._Wok;
        int pr = 0;
        string Id = "";
        string name = "WOK";
        string compos = "";
        
        //M1
        if(udon.state == true)
        {
            pr += wok[udon.id].Price;
            Id += udon.id;
            compos += wok[udon.id].Name;
        }
        if (soba.state == true)
        {
            pr += wok[soba.id].Price;
            Id += soba.id;
            compos += wok[soba.id].Name;
        }
        if (funch.state == true)
        {
            pr += wok[funch.id].Price;
            Id += funch.id;
            compos += wok[funch.id].Name;
        }
        if (ramen.state == true)
        {
            pr += wok[ramen.id].Price;
            Id += ramen.id;
            compos += wok[ramen.id].Name;
        }
        //M2
        if (netO.state == true)
        {
            pr += wok[netO.id].Price;
            Id += "-"+netO.id;
            compos += ", "+wok[netO.id].Name;
        }
        if (daO.state == true)
        {
            pr += wok[daO.id].Price;
            Id += "-" + daO.id;
            compos += ", " + wok[daO.id].Name;
        }
        //M3
        if (netS.state == true)
        {
            pr += wok[netS.id].Price;
            Id += "-" + netS.id;
            compos += ", " + wok[netS.id].Name;
        }
        if (ostr.state == true)
        {
            pr += wok[ostr.id].Price;
            Id += "-" + ostr.id;
            compos += ", " + wok[ostr.id].Name;
        }
        if (slch.state == true)
        {
            pr += wok[slch.id].Price;
            Id += "-" + slch.id;
            compos += ", " + wok[slch.id].Name;
        }
        if (tai.state == true)
        {
            pr += wok[tai.id].Price;
            Id += "-" + tai.id;
            compos += ", " + wok[tai.id].Name;
        }
        if (ter.state == true)
        {
            pr += wok[ter.id].Price;
            Id += "-" + ter.id;
            compos += ", " + wok[ter.id].Name;
        }
        if (firm.state == true)
        {
            pr += wok[firm.id].Price;
            Id += "-" + firm.id;
            compos += ", " + wok[firm.id].Name;
        }

        //M4
        string mid = "-0";
        if (gov.state == true)
        {
            pr += wok[gov.id].Price;
            mid = "-"+gov.id;
            compos += ", " + wok[gov.id].Name;
        }
        if (kur.state == true)
        {
            pr += wok[kur.id].Price;
            mid = "-" + kur.id;
            compos += ", " + wok[kur.id].Name;
        }
        if (los.state == true)
        {
            pr += wok[los.id].Price;
            mid = "-" + los.id;
            compos += ", " + wok[los.id].Name;
        }
        if (mk.state == true)
        {
            pr += wok[mk.id].Price;
            mid = "-" + mk.id;
            compos += ", " + wok[mk.id].Name;
        }
        if (svin.state == true)
        {
            pr += wok[svin.id].Price;
            mid = "-" + svin.id;
            compos += ", " + wok[svin.id].Name;
        }
        Id += mid;
        //M5
        string topId = "-0";
        if (kunsh.state == true)
        {
            pr += wok[kunsh.id].Price;
            compos += ", " + wok[kunsh.id].Name;
            if (topId == "-0")
            {
                topId = "-" + kunsh.id;
            }
            else
            {
                topId += "*" + kunsh.id;
            }
        }
        if (moza.state == true)
        {
            pr += wok[moza.id].Price;
            compos += ", " + wok[moza.id].Name;
            if (topId == "-0")
            {
                topId = "-" + moza.id;
            }
            else
            {
                topId += "*" + moza.id;
            }
        }
        if (asort.state == true)
        {
            pr += wok[asort.id].Price;
            compos += ", " + wok[asort.id].Name;
            if (topId == "-0")
            {
                topId = "-" + asort.id;
            }
            else
            {
                topId += "*" + asort.id;
            }
        }
        if (ogur.state == true)
        {
            pr += wok[ogur.id].Price;
            compos += ", " + wok[ogur.id].Name;
            if (topId == "-0")
            {
                topId = "-" + ogur.id;
            }
            else
            {
                topId += "*" + ogur.id;
            }
        }
        if (pamez.state == true)
        {
            pr += wok[pamez.id].Price;
            compos += ", " + wok[pamez.id].Name;
            if (topId == "-0")
            {
                topId = "-" + pamez.id;
            }
            else
            {
                topId += "*" + pamez.id;
            }
        }
        if (fasol.state == true)
        {
            pr += wok[fasol.id].Price;
            compos += ", " + wok[fasol.id].Name;
            if (topId == "-0")
            {
                topId = "-" + fasol.id;
            }
            else
            {
                topId += "*" + fasol.id;
            }
        }
        if (suc.state == true)
        {
            pr += wok[suc.id].Price;
            compos += ", " + wok[suc.id].Name;
            if (topId == "-0")
            {
                topId = "-" + suc.id;
            }
            else
            {
                topId += "*" + suc.id;
            }
        }
        if (sher.state == true)
        {
            pr += wok[sher.id].Price;
            compos += ", " + wok[sher.id].Name;
            if (topId == "-0")
            {
                topId = "-" + sher.id;
            }
            else
            {
                topId += "*" + sher.id;
            }
        }
        if (shamp.state == true)
        {
            pr += wok[shamp.id].Price;
            compos += ", " + wok[shamp.id].Name;
            if (topId == "-0")
            {
                topId = "-" + shamp.id;
            }
            else
            {
                topId += "*" + shamp.id;
            }
        }
        if (egg.state == true)
        {
            pr += wok[egg.id].Price;
            compos += ", " + wok[egg.id].Name;
            if (topId == "-0")
            {
                topId = "-" + egg.id;
            }
            else
            {
                topId += "*" + egg.id;
            }
        }
        Id += topId;
        //M6
        if (netS1.state == true)
        {
            pr += wok[netS1.id].Price;
            compos += ", " + wok[netS1.id].Name;
            Id += "-" + netS1.id;
        }
        if (ostr1.state == true)
        {
            pr += wok[ostr1.id].Price;
            compos += ", " + wok[ostr1.id].Name;
            Id += "-" + ostr1.id;
        }
        if (slch1.state == true)
        {
            pr += wok[slch1.id].Price;
            compos += ", " + wok[slch1.id].Name;
            Id += "-" + slch1.id;
        }
        if (tai1.state == true)
        {
            pr += wok[tai1.id].Price;
            compos += ", " + wok[tai1.id].Name;
            Id += "-" + tai1.id;
        }
        if (ter1.state == true)
        {
            pr += wok[ter1.id].Price;
            compos += ", " + wok[ter1.id].Name;
            Id += "-" + ter1.id;
        }
        if (firm1.state == true)
        {
            pr += wok[firm1.id].Price;
            compos += ", " + wok[firm1.id].Name;
            Id += "-" + firm1.id;
        }
        
        zw.IdWok = Id;
        zw.Composition = compos;
        zw.Price = pr;
        zw.Name = name;
        GetComponent<Text>().text = "Заказать " + pr + " ₽";
    }
}
