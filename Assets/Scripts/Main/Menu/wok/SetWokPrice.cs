﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetWokPrice : MonoBehaviour
{
    [Space]
    [Header("Лапша")]
    public Text udonP;
    public Text sobaP;
    public Text funceP;
    public Text ramenP;
    [Space]
    [Header("Мясо")]
    public Text goviadP;
    public Text kuraP;
    public Text lososP;
    public Text mkP;
    public Text svinP;
    [Space]
    [Header("Топинг")]
    public Text kungP;
    public Text mocarP;
    public Text ovoasortP;
    public Text ogurP;
    public Text parmeP;
    public Text fasolP;
    public Text sukiniP;
    public Text cheriP;
    public Text shampP;
    public Text eggP;
    [Space]
    [Header("Соус")]
    public Text ostrP;
    public Text slivP;
    public Text taiP;
    public Text tereP;
    public Text firmP;
    void Start()
    {
        var wok = GameObject.Find("Hud").GetComponent<Hud>()._Wok;

        udonP.text = wok[32].Price+ " ₽";
        sobaP.text = wok[33].Price + " ₽";
        ramenP.text = wok[34].Price + " ₽";
        funceP.text = wok[35].Price + " ₽";

        goviadP.text = wok[51].Price + " ₽";
        kuraP.text = wok[50].Price + " ₽";
        lososP.text = wok[53].Price + " ₽";
        mkP.text = wok[52].Price + " ₽";
        svinP.text = wok[54].Price + " ₽";

        kungP.text = wok[64].Price + " ₽";
        mocarP.text = wok[61].Price + " ₽";
        ovoasortP.text = wok[59].Price + " ₽";
        ogurP.text = wok[56].Price + " ₽";
        parmeP.text = wok[62].Price + " ₽";
        fasolP.text = wok[58].Price + " ₽";
        sukiniP.text = wok[55].Price + " ₽";
        cheriP.text = wok[57].Price + " ₽";
        shampP.text = wok[60].Price + " ₽";
        eggP.text = wok[63].Price + " ₽";
        ostrP.text = wok[45].Price + " ₽";
        slivP.text = wok[46].Price + " ₽";
        taiP.text = wok[47].Price + " ₽";
        tereP.text = wok[48].Price + " ₽";
        firmP.text = wok[49].Price + " ₽";
    }

}
