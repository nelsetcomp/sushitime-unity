﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptPizzaH : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    [SerializeField]
    public GameObject scrollItemPrefab;
    private Hud hud;

    void Start()
    {
        hud = GameObject.Find("Hud").GetComponent<Hud>();

        foreach (var r in hud._Pizza)
        {
            generateItem(r.Id, r.Name, r.Composition, r.PriceA, r.WeightA, r.PriceB, r.WeightB);
        }
    }

    void generateItem(string id, string name, string composition, string priceA, string weightA, string priceB, string weightB)
    {
        var scrollItem = Instantiate(scrollItemPrefab);
        scrollItem.transform.SetParent(scrollContent.transform, false);
        scrollItem.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().menuAtlas.GetSprite(id);
        scrollItem.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Text>().text = name;
        scrollItem.transform.GetChild(0).transform.GetChild(2).transform.GetComponent<Text>().text = composition;
        scrollItem.transform.GetChild(0).transform.GetChild(3).transform.GetComponent<Text>().text = priceA + " ₽";
        scrollItem.transform.GetChild(0).transform.GetChild(5).transform.GetComponent<Text>().text = priceB + " ₽";
        scrollItem.transform.GetChild(0).transform.GetChild(4).transform.GetComponent<AddVKPizza>().Id = id + ":";
        scrollItem.transform.GetChild(0).transform.GetChild(6).transform.GetComponent<AddVKPizza>().Id = id + "::";
        if (priceB == "0")
        {
            scrollItem.transform.GetChild(0).transform.GetChild(5).transform.gameObject.SetActive(false);
            scrollItem.transform.GetChild(0).transform.GetChild(6).transform.gameObject.SetActive(false);
        }
        //scrollItem.GetComponent<Button>().onClick.AddListener(delegate { clicL(cityId, fileName, cityName, cLat, cLon); });
    }
}
