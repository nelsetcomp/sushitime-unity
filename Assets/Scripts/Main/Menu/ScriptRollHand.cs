﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptRollHand : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    [SerializeField]
    public GameObject scrollItemPrefab;
    private Hud hud;

    void Start()
    {
        hud = GameObject.Find("Hud").GetComponent<Hud>();

        foreach(var r in hud._Roll)
        {
            generateItem(r.Id, r.Name, r.Composition, r.Weight, r.Price, r.Hot, r.Spicy, r.Vegan, r.Classic);
        }
    }

    void generateItem(string id, string name, string composition, string weight, string price, string hot, string spicy, string vegan, string classic)
    {
        var scrollItem = Instantiate(scrollItemPrefab);
        scrollItem.transform.SetParent(scrollContent.transform, false);
        scrollItem.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().menuAtlas.GetSprite(id);
        scrollItem.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Text>().text = name;
        scrollItem.transform.GetChild(0).transform.GetChild(2).transform.GetComponent<Text>().text = composition;
        scrollItem.transform.GetChild(0).transform.GetChild(3).transform.GetComponent<Text>().text = weight + " гр";
        scrollItem.transform.GetChild(0).transform.GetChild(4).transform.GetComponent<Text>().text = price + " ₽";
        scrollItem.transform.GetChild(0).transform.GetChild(5).transform.GetComponent<AddVCorz>().Id = id;

        if (hot == "1")
        {
            var ht = new GameObject("h");
            ht.transform.SetParent(scrollItem.transform.GetChild(0).transform.GetChild(6).transform);
            ht.AddComponent<Image>();
            ht.GetComponent<RectTransform>().sizeDelta = new Vector2(72, 72);
            ht.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            ht.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().hot;
        }
        if (spicy == "1")
        {
            var sp = new GameObject("s");
            sp.transform.SetParent(scrollItem.transform.GetChild(0).transform.GetChild(6).transform);
            sp.AddComponent<Image>();
            sp.GetComponent<RectTransform>().sizeDelta = new Vector2(72, 72);
            sp.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            sp.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().spicy;
        }
        if (vegan == "1")
        {
            var ve = new GameObject("v");
            ve.transform.SetParent(scrollItem.transform.GetChild(0).transform.GetChild(6).transform);
            ve.AddComponent<Image>();
            ve.GetComponent<RectTransform>().sizeDelta = new Vector2(72, 72);
            ve.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            ve.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().vegan;
        }
        if (classic == "1")
        {
            var cl = new GameObject("c");
            cl.transform.SetParent(scrollItem.transform.GetChild(0).transform.GetChild(6).transform);
            cl.AddComponent<Image>();
            cl.GetComponent<RectTransform>().sizeDelta = new Vector2(164, 72);
            cl.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            cl.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().classic;
        }

        //scrollItem.GetComponent<Button>().onClick.AddListener(delegate { clicL(cityId, fileName, cityName, cLat, cLon); });
    }
    void clicL()
    {
        
    }
}
