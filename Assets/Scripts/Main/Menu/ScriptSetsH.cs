﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptSetsH : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    [SerializeField]
    public GameObject scrollItemPrefab;
    private Hud hud;

    void Start()
    {
        hud = GameObject.Find("Hud").GetComponent<Hud>();

        foreach (var r in hud._Sets)
        {
            generateItem(r.Id, r.Name, r.Composition, r.Weight, r.Price);
        }
    }

    void generateItem(string id, string name, string composition, string weight, string price)
    {
        var scrollItem = Instantiate(scrollItemPrefab);
        scrollItem.transform.SetParent(scrollContent.transform, false);
        scrollItem.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Image>().sprite = GameObject.Find("MainController").GetComponent<MainController>().menuAtlas.GetSprite(id);
        scrollItem.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Text>().text = name;
        scrollItem.transform.GetChild(0).transform.GetChild(2).transform.GetComponent<Text>().text = composition;
        scrollItem.transform.GetChild(0).transform.GetChild(3).transform.GetComponent<Text>().text = weight + " гр";
        scrollItem.transform.GetChild(0).transform.GetChild(4).transform.GetComponent<Text>().text = price + " ₽";
        scrollItem.transform.GetChild(0).transform.GetChild(5).transform.GetComponent<AddVCorz>().Id = id;
        //scrollItem.GetComponent<Button>().onClick.AddListener(delegate { clicL(cityId, fileName, cityName, cLat, cLon); });
    }
}
