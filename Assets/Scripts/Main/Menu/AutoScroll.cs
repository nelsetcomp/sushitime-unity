﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AutoScroll : MonoBehaviour
{
    public ScrollRectFaster scr;
    
    float pos;
    bool go = false;


    // Start is called before the first frame update
    public void Go(float poss)
    {
        
        pos = poss;
        go = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (go == true)
        {
            if(scr.normalizedPosition.x < pos)
            {
                scr.normalizedPosition = new Vector2(scr.normalizedPosition.x + 0.01f, scr.normalizedPosition.y);
            }
            if (scr.normalizedPosition.x > pos)
            {
                scr.normalizedPosition = new Vector2(scr.normalizedPosition.x - 0.01f, scr.normalizedPosition.y);
            }
            if ((float)Math.Round(scr.normalizedPosition.x, 2) == pos)
            {
                go = false;
            }
        }
       
        
    }
   
}
