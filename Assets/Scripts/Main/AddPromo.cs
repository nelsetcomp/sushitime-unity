﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPromo : MonoBehaviour
{
    [SerializeField] private Transform _content;
    [SerializeField] private GameObject _promoPrefab;
    private Hud _hud => FindObjectOfType<Hud>();
    private void Start()
    {
        foreach (var item in _hud._PromoId)
        {
            var _promo = Instantiate(_promoPrefab, _content, false);
            _promo.GetComponent<AddVCorz>().Id = item;
            _promo.GetComponent<InetImage>().GetImage($"{_hud._PromoUrl}{item}.jpg");
        }
    }
}
