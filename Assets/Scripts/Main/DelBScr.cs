﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DelBScr : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Sprite s1;
    public Sprite s2;
    string id;

    public string Id { get => id; set => id = value; }

    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
    public void clic()
    {
        StartCoroutine(ExecuteAfterTime(0.2f));
    }
    IEnumerator ExecuteAfterTime(float timeInSec)
    {
        transform.GetComponent<Image>().sprite = s2;

        yield return new WaitForSeconds(timeInSec);
        //сделать нужное
        transform.GetComponent<Image>().sprite = s1;

        var mc = GameObject.Find("MainController").GetComponent<MainCorzina>();
        int i = 0;
        for(int x = 0; x < mc.MainTovar.Count; x++)
        {
            if(mc.MainTovar[x] == Id)
            {
                i = x;
                break;
            }
        }
        mc.MainTovar.RemoveAt(i);

        mc.DicTovar[Id].Count--;
        if (mc.DicTovar[Id].Count <= 0)
        {
            mc.DicTovar.Remove(Id);
            Destroy(transform.parent.transform.parent.gameObject);
        }
        else
        {
            transform.parent.transform.GetChild(5).transform.GetComponent<Text>().text = mc.DicTovar[Id].Count + "";
            transform.parent.transform.GetChild(1).transform.GetComponent<Text>().text = mc.DicTovar[Id].Price * mc.DicTovar[Id].Count + " ₽";
        }
        mc.SummPrice();
    }
}
