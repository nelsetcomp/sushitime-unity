﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CorzGoTime : MonoBehaviour
{
    public Button zakazB;
    public GameObject im;
    public Text tx;
    private void Awake()
    {
        var t = new DateTime();
        t = DateTime.Now;
        
        if(t.Hour == 22 || t.Hour == 23)
        {
            zakazB.interactable = false;
            im.SetActive(true);
            tx.text = "Извините, но мы работаем до 22.00. Спасибо за понимание!";
        }
        else if (t.Hour >= 0 && t.Hour < 8)
        {
            zakazB.interactable = false;
            im.SetActive(true);
            tx.text = "Извините, но мы работаем с 10:00. Вы можете оставить предварительный заказ с 08:00.";
        }
        else if (t.Hour >= 8 && t.Hour < 10)
        {
            zakazB.interactable = true;
            im.SetActive(true);
            tx.text = "Извините, но мы работаем с 10:00. Сейчас вы можете сделать предварительный заказ, а наш менеджер свяжется с Вами в рабочее время!";
        }
    }
}
