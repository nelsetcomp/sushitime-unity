﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BotMenuBut : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Sprite a1;
    public Sprite a2;
    public GameObject o1;
    public GameObject o2;

    public GameObject mainScreen;
    public GameObject an1Screen;
    public GameObject an2Screen;
    public void OnPointerClick(PointerEventData eventData)
    {
        
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        Clic();
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
    public void Clic()
    {
        transform.GetChild(0).transform.GetComponent<Image>().sprite = a2;
        transform.GetChild(1).transform.GetComponent<Text>().color = Color.red;

        o1.GetComponent<BotMenuBut>().UnClic();
        o2.GetComponent<BotMenuBut>().UnClic();

        mainScreen.SetActive(true);
        an1Screen.SetActive(false);
        an2Screen.SetActive(false);
    }
    public void UnClic()
    {
        transform.GetChild(0).transform.GetComponent<Image>().sprite = a1;
        transform.GetChild(1).transform.GetComponent<Text>().color = new Color(0.509804f, 0.5058824f, 0.509804f, 1);
    }
    IEnumerator ExecuteAfterTime(float timeInSec)
    {
        
        yield return new WaitForSeconds(timeInSec);
        //сделать нужное
        
    }
}
