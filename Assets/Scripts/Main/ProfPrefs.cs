﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfPrefs : MonoBehaviour
{
    public string prefsName;
    void Start()
    {
        
      GetComponent<InputField>().text = PlayerPrefs.GetString(prefsName);
        
    }

    public void Ut()
    {
        PlayerPrefs.SetString(prefsName, GetComponent<InputField>().text);
    }
}
