﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OplatBut : MonoBehaviour, IPointerClickHandler
{
    public Sprite s1;
    public Sprite s2;
    public GameObject b2;
    public bool state = true;

    

    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    
    public void clic()
    {
        if (state == false)
        {
            state = true;
            transform.GetComponent<Image>().sprite = s2;
            transform.GetChild(0).transform.GetComponent<Text>().color = Color.white;
            b2.GetComponent<OplatBut>().Unclic();
        }
    }
    public void Unclic()
    {
        state = false;
        transform.GetComponent<Image>().sprite = s1;
        transform.GetChild(0).transform.GetComponent<Text>().color = new Color(0.509804f, 0.5058824f, 0.509804f, 1);

    }
}
