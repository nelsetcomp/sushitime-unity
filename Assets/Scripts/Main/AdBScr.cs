﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AdBScr : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Sprite s1;
    public Sprite s2;
    string id;

    public string Id { get => id; set => id = value; }

    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
    public void clic()
    {
        StartCoroutine(ExecuteAfterTime(0.2f));
    }
    IEnumerator ExecuteAfterTime(float timeInSec)
    {
        transform.GetComponent<Image>().sprite = s2;
        
        yield return new WaitForSeconds(timeInSec);
        //сделать нужное
        transform.GetComponent<Image>().sprite = s1;
        
        var mc = GameObject.Find("MainController").GetComponent<MainCorzina>();
        mc.MainTovar.Add(Id);
        mc.DicTovar[Id].Count++;
        transform.parent.transform.GetChild(5).transform.GetComponent<Text>().text = mc.DicTovar[Id].Count + "";
        transform.parent.transform.GetChild(1).transform.GetComponent<Text>().text = mc.DicTovar[Id].Price * mc.DicTovar[Id].Count + " ₽";
        mc.SummPrice();
    }
}
