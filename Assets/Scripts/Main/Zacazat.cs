﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Zacazat : MonoBehaviour
{
    public GameObject c2;
    public GameObject c3;

    public GameObject p1;
    public GameObject p2;
    public Text lzOr;

    public InputField comment;
    public InputField change;
    public InputField time;
    public void Zacaz()
    {
        NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);
        queryString.Add("platform", Application.platform + "");
        queryString.Add("email", PlayerPrefs.GetString("email"));
        queryString.Add("first_name", PlayerPrefs.GetString("name"));
        queryString.Add("phone", PlayerPrefs.GetString("phone"));
        queryString.Add("city", GameObject.Find("Hud").GetComponent<Hud>()._City);
        queryString.Add("street1", PlayerPrefs.GetString("street"));
        queryString.Add("house", PlayerPrefs.GetString("house"));
        queryString.Add("kv", PlayerPrefs.GetString("num"));
        queryString.Add("person", PlayerPrefs.GetString("person"));
        int opl = 1;
        if(p1.GetComponent<OplatBut>().state == true)
        {
            opl = 2;
        }
        if (p2.GetComponent<OplatBut>().state == true)
        {
            opl = 1;
        }
        string t = "";
        t = (time.text != "") ? time.text : DateTime.Now.AddHours(1).ToShortTimeString();
        queryString.Add("time", t);
        queryString.Add("change", change.text);
        queryString.Add("payment", opl+"");
        queryString.Add("comment", comment.text);

        string menu = "";
        foreach (var m in GameObject.Find("MainController").GetComponent<MainCorzina>().DicTovar)
        {
            if(m.Value.Menu == "wok")
            {
                menu += "119" + "-" + m.Value.Count + "-" + m.Key+ "_";
            }
            else
            {
                menu += m.Key + "-" + m.Value.Count + "_";
            }
            
        }
        menu = menu.Remove(menu.Length - 1);
        queryString.Add("menu", menu);

        //StartCoroutine(GetRequestMenu("http://gallery.nelset.com" + "/appsorder?"+queryString));
        StartCoroutine(GetRequestMenu(GameObject.Find("Hud").GetComponent<Hud>()._Url + "/appsorder?" + queryString));
    }

    IEnumerator GetRequestMenu(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            c3.SetActive(true);
            var t = GameObject.Find("TruZacazText").GetComponent<Text>();

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
                t.text = "Неудалось отправить заказ. Попробуйте сделать заказ еще раз.";
            }
            else
            {
                t.text = "Спасибо за Ваш заказ! В ближайшее время наш оператор свяжется с Вами для подтверждения.\n \nНомер заказа: " + webRequest.downloadHandler.text;
                PlayerPrefs.SetString("last_order", webRequest.downloadHandler.text);
                lzOr.text = webRequest.downloadHandler.text;
                GameObject.Find("MainController").GetComponent<MainCorzina>().ClearCorz();
            }
            c2.SetActive(false);
        }
    }
}
