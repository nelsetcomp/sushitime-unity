﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PoliB : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Sprite s1;
    public Sprite s2;
    public void OnPointerClick(PointerEventData eventData)
    {
        clic();
    }
    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
    public void clic()
    {
        StartCoroutine(ExecuteAfterTime(0.2f));
    }
    IEnumerator ExecuteAfterTime(float timeInSec)
    {
        
        transform.GetComponent<Image>().sprite = s2;
        transform.GetChild(0).transform.GetComponent<Text>().color = Color.white;
        yield return new WaitForSeconds(timeInSec);
        //сделать нужное
        transform.GetComponent<Image>().sprite = s1;
        transform.GetChild(0).transform.GetComponent<Text>().color = new Color(0.509804f, 0.5058824f, 0.509804f, 1);
    }
}
