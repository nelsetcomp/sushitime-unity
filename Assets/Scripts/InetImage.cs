﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InetImage : MonoBehaviour
{
    //public object DataTime { get; private set; }
    private Image image => transform.GetChild(0).GetComponent<Image>();
    public void GetImage(string url)
    {
        StartCoroutine(DownloadImage(url));
    }

    private IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            // ImageComponent.texture = ((DownloadHandlerTexture) request.downloadHandler).texture;

            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Rect rect = new Rect(0, 0, tex.width, tex.height);
            
            Sprite sprite = Sprite.Create(tex, rect, new Vector2(tex.width/2, tex.height/2), 1024);
            image.rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
            image.sprite = sprite;
        }
    }
}
