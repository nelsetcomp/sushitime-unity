﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{
    string city;
    string linkmenu;
    string linkwok;
    string url;
    string minorder;
    string policy;
    string promoUrl;
    List<string> promoId = new List<string>();

    List<string> cityList = new List<string>();
    List<AllT> alltovar = new List<AllT>();
    List<Roll> roll = new List<Roll>();
    List<Roll> grill_roll = new List<Roll>();
    List<Roll> hot_roll = new List<Roll>();
    List<Roll> spicy_roll = new List<Roll>();
    List<Roll> sushi = new List<Roll>();
    List<Roll> sets = new List<Roll>();
    List<Pizza> pizza = new List<Pizza>();
    List<Napitki> napitki = new List<Napitki>();

    public string _City { get => city; set => city = value; }
    public string _Linkmenu { get => linkmenu; set => linkmenu = value; }
    public string _Url { get => url; set => url = value; }
    public string _Minorder { get => minorder; set => minorder = value; }
    public string _Policy { get => policy; set => policy = value; }
    public string _Linkwok { get => linkwok; set => linkwok = value; }
    public List<Roll> _Roll { get => roll; set => roll = value; }
    public List<Roll> _Grill_roll { get => grill_roll; set => grill_roll = value; }
    public List<Roll> _Hot_roll { get => hot_roll; set => hot_roll = value; }
    public List<Roll> _Spicy_roll { get => spicy_roll; set => spicy_roll = value; }
    public List<Roll> _Sushi { get => sushi; set => sushi = value; }
    public List<Roll> _Sets { get => sets; set => sets = value; }
    public List<Pizza> _Pizza { get => pizza; set => pizza = value; }
    public List<Napitki> _Napitki { get => napitki; set => napitki = value; }
    public List<string> CityList { get => cityList; set => cityList = value; }
    public List<AllT> _Alltovar { get => alltovar; set => alltovar = value; }
    public Dictionary<int, WOK> _Wok { get => wok; set => wok = value; }
    public string _PromoUrl { get => promoUrl; set => promoUrl = value; }
    public List<string> _PromoId { get => promoId; set => promoId = value; }

    private Dictionary<int, WOK> wok = new Dictionary<int, WOK>();
    public class WOK
    {
        string name;
        int price;

        public string Name { get => name; set => name = value; }
        public int Price { get => price; set => price = value; }
    }

    public class Napitki
    {
        string id;
        string name;
        string price;
        string weight;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Price { get => price; set => price = value; }
        public string Weight { get => weight; set => weight = value; }
    }
    public class Pizza
    {
        string id;
        string name;
        string menu;
        string composition;
        string priceA;
        string weightA;
        string priceB;
        string weightB;
        string spicy;
        string vegan;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Menu { get => menu; set => menu = value; }
        public string Composition { get => composition; set => composition = value; }
        public string PriceA { get => priceA; set => priceA = value; }
        public string WeightA { get => weightA; set => weightA = value; }
        public string PriceB { get => priceB; set => priceB = value; }
        public string WeightB { get => weightB; set => weightB = value; }
        public string Spicy { get => spicy; set => spicy = value; }
        public string Vegan { get => vegan; set => vegan = value; }
    }
    public class Roll
    {
        string id;
        string name;
        string composition;
        string price;
        string weight;
        string spicy;
        string vegan;
        string hot;
        string grill;
        string menu;
        string classic;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Composition { get => composition; set => composition = value; }
        public string Price { get => price; set => price = value; }
        public string Weight { get => weight; set => weight = value; }
        public string Spicy { get => spicy; set => spicy = value; }
        public string Vegan { get => vegan; set => vegan = value; }
        public string Hot { get => hot; set => hot = value; }
        public string Grill { get => grill; set => grill = value; }
        public string Menu { get => menu; set => menu = value; }
        public string Classic { get => classic; set => classic = value; }
    }
    public class AllT
    {
        string id;
        string name;
        string menu;
        string composition;
        string price;
        string priceA;
        string weightA;
        string priceB;
        string weightB;
        string spicy;
        string vegan;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Menu { get => menu; set => menu = value; }
        public string Composition { get => composition; set => composition = value; }
        public string PriceA { get => priceA; set => priceA = value; }
        public string WeightA { get => weightA; set => weightA = value; }
        public string PriceB { get => priceB; set => priceB = value; }
        public string WeightB { get => weightB; set => weightB = value; }
        public string Spicy { get => spicy; set => spicy = value; }
        public string Vegan { get => vegan; set => vegan = value; }
        public string Price { get => price; set => price = value; }
    }
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
