﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour
{
    string _reqvest;
    string _menu;
    string _wok;
    CityList _setingLsit = new CityList();
    Hud hud;

    void Start()
    {
        if (PlayerPrefs.GetString("city") == "" || PlayerPrefs.GetString("city") == null)
        {
            PlayerPrefs.SetString("city", "Ставрополь");
        }

        hud = GameObject.Find("Hud").GetComponent<Hud>();
        hud._City = PlayerPrefs.GetString("city");

        StartCoroutine(GetRequest("https://sushitime.ru/json/config.json"));
    }
    void PasWok()
    {
        var serialW = new SerialWok();
        serialW.Serial(_wok);

        SceneManager.LoadScene("Main");
    }
    void PasMenu()
    {
        var serialM = new SerialMenu();
        serialM.Serial(_menu);

        StartCoroutine(GetRequestWok(hud._Linkwok));
    }
    void GoPrs()
    {
        _reqvest = "{\"Value\":" + _reqvest + "}";

        _setingLsit = JsonUtility.FromJson<CityList>(_reqvest);

        foreach (var r in _setingLsit.Value)
        {
            hud.CityList.Add(r.city);

            if (hud._City == r.city)
            {
                hud._Linkmenu = r.linkmenu;
                hud._Url = r.url;
                hud._Minorder = r.minorder;
                hud._Policy = r.policy;
                hud._Linkwok = r.linkwok;
                hud._PromoUrl = r.promourl;
                hud._PromoId = AddPromoId(r.promoid);
            }
        }
        StartCoroutine(GetRequestMenu(hud._Linkmenu));
    }
    private List<string> AddPromoId(string textId) 
    {
        var list = new List<string>();
        var strList = textId.Split(';');
        foreach (var item in strList)
        {
            list.Add(item);
        }
        return list;        
    }
    IEnumerator GetRequestWok(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                _wok = webRequest.downloadHandler.text;

                PasWok();
            }
        }
    }
    IEnumerator GetRequestMenu(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                _menu = webRequest.downloadHandler.text;
                
                PasMenu();
            }
        }
    }
    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            

            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                _reqvest = webRequest.downloadHandler.text;
                GoPrs();
                
            }
        }
    }

    [Serializable]
    public class CityList
    {
        public List<Value> Value = new List<Value>();
    }
    [Serializable]
    public class Value
    {
        public string city;
        public string linkmenu;
        public string url;
        public string minorder;
        public string policy;
        public string linkwok;
        public string promoid;
        public string promourl;
    }
}
