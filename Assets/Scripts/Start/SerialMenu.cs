﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerialMenu : MonoBehaviour
{
    MenuList _menuLsit = new MenuList();
    public void Serial(string _reqv)
    {
        var hud = GameObject.Find("Hud").GetComponent<Hud>();
       
        _reqv = "{\"Value\":" + _reqv + "}";
       
        _menuLsit = JsonUtility.FromJson<MenuList>(_reqv);

        foreach(var m in _menuLsit.Value)
        {
            hud._Alltovar.Add(
                new Hud.AllT 
                { 
                    Id = m.id, 
                    Name = m.name, 
                    Price = m.price, 
                    Composition = m.composition, 
                    Spicy = m.spicy, 
                    Vegan = m.vegan, 
                    Menu = m.menu, 
                    PriceA = m.sizeaprice, 
                    WeightA = m.sezeaweight, 
                    PriceB = m.sizebprice, 
                    WeightB = m.sezebweight 
                });

            if (m.menu.Equals("rolly") && m.hot.Equals("0") && m.grill.Equals("0") && m.spicy.Equals("0"))
            {
                hud._Roll.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu
                    });
            }
            if (m.menu.Equals("rolly") && m.hot.Equals("0") && m.grill.Equals("1") && m.spicy.Equals("0"))
            {
                hud._Grill_roll.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu 
                    });
            }
            if (m.menu.Equals("rolly") && m.hot.Equals("1") && m.grill.Equals("0") && m.spicy.Equals("0"))
            {
                hud._Hot_roll.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu 
                    });
            }
            if (m.menu.Equals("rolly") && m.hot.Equals("0") && m.grill.Equals("0") && m.spicy.Equals("1"))
            {
                hud._Spicy_roll.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu 
                    });
            }
            if (m.menu.Equals("sushi"))
            {
                hud._Sushi.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu 
                    });
            }
            if (m.menu.Equals("sets"))
            {
                hud._Sets.Add(
                    new Hud.Roll 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Price = m.price, 
                        Weight = m.weight, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Hot = m.hot, 
                        Grill = m.grill, 
                        Classic = m.classic, 
                        Menu = m.menu 
                    });
            }
            if (m.menu.Equals("pizza"))
            {
                hud._Pizza.Add(
                    new Hud.Pizza 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Composition = m.composition, 
                        Spicy = m.spicy, 
                        Vegan = m.vegan, 
                        Menu = m.menu, 
                        PriceA = m.sizeaprice, 
                        WeightA = m.sezeaweight, 
                        PriceB = m.sizebprice, 
                        WeightB = m.sezebweight 
                    });
            }
            if (m.menu.Equals("napitki"))
            {
                hud._Napitki.Add(
                    new Hud.Napitki 
                    { 
                        Id = m.id, 
                        Name = m.name, 
                        Price = m.price, 
                        Weight = m.weight 
                    });
            }
        }
    }
    [Serializable]
    public class MenuList
    {
        public List<Value> Value = new List<Value>();
    }
    [Serializable]
    public class Value
    {
        public string name;
        public string id;
        public string price;
        public string composition;
        public string menu;
        public string weight;
        public string volume;
        public string own;
        public string spicy;
        public string hot;
        public string classic;
        public string grill;
        public string vegan;
        public string sizea;
        public string sizeaprice;
        public string sezeaweight;
        public string sizeb;
        public string sizebprice;
        public string sezebweight;
        public string count;
    }
}
