﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerialWok : MonoBehaviour
{
    MenuList _menuLsit = new MenuList();
    public void Serial(string _reqv)
    {
        var hud = GameObject.Find("Hud").GetComponent<Hud>();
        _reqv = "{\"Value\":" + _reqv + "}";
        _menuLsit = JsonUtility.FromJson<MenuList>(_reqv);
        foreach (var m in _menuLsit.Value)
        {
            hud._Wok.Add(m.id, new Hud.WOK { Name = m.name, Price = int.Parse(m.price) });
        }
    }
    [Serializable]
    public class MenuList
    {
        public List<Value> Value = new List<Value>();
    }
    [Serializable]
    public class Value
    {
        public int id;
        public string name;
        public string price;
    }
}
